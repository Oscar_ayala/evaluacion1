/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Fabian
 */
public class Concatenador {

    public int getSuma() {
        return suma;
    }

    public void setSuma(int suma) {
        this.suma = suma;
    }
    private int numero1;
    private int numero2;
    private int numero3;
    private int resultado;

    private int suma;

    /**
     * @return the n1
     */

    public int getNumero1() {
        return numero1;
    }

    public void setNumero1(int numero1) {
        this.numero1 = numero1;
    }

    public int getNumero2() {
        return numero2;
    }

    public void setNumero2(int numero2) {
        this.numero2 = numero2;
    }

    public int getNumero3() {
        return numero3;
    }

    public void setNumero3(int numero3) {
        this.numero3 = numero3;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public int sumar() {

        //si los valores son mayores a 1000 entonces rechazar
        resultado = this.getNumero1()* this.getNumero2()* this.getNumero3();

        resultado= resultado / 100;

        return resultado;

        // Calculo del interés sobre el capital. 
    }

    public int sumar(int n1, int n2, int n3, int n4) {
        this.setNumero1(n1);
        this.setNumero2(n2);
        this.setNumero3(n3);
        this.setResultado(n4);

        return sumar();

    }

}
